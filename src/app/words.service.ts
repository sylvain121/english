import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Quizz } from './Quizz';

@Injectable({
  providedIn: 'root'
})
export class WordsService {


  private list = [
    ["Whether", "si"],
    ["However", "cependant"],
    ["Therefore", "donc"],
    ["Moreover", "de plus"],
    ["Further", "plus loin"],
    ["Thought", "pensée"],
    ["Whenever", "chaque fois que"],
    ["In spite of", "malgrés"],
    ["Chatty", "bavard"],
    ["Heavy", "lourd"],
    ["as well", "également"],
    ["Involves", "impliques"],
    ["Structural flaws", "defauts structurel"],
    ["Outages", "pannes"],
    ["as a whole", "tout entier"],
    ["Nurture", "entretenir"],
    ["Cattle", "bétail"],
    ["Trade-off", "compromis"],
    ["Matter", "probleme"],
    ["Worse", "pire"],
    ["Menial", "subalterne"],
    ["Perk", "avantages"],
    ["Roughly", "a peu prêt"],
    ["Meant", "censée"],
    ["Nor", "ni"],
    ["Rough", "grossier"],
    ["Sustainability", "durabilité"],
    ["Whereas", "alors que"],
    ["Throughout", "tout au long de"],
    ["Neither", "ni l'un ni l'autre"],
    ["Heard", "entendue"],
    ["Confidence", "confiance"],
    ["Erred", "s'est trompé"],
    ["Forth", "en avant"],
    ["Sewing", "coudre"],
    ["light bulbs", "ampoules"],
    ["Though", "bien que"],
    ["it lasts 1 hour", " ça dure 1 heure"],
    ["on average", "en moyenne"],
    ["to produce", "produire"],
    ["once", "Une fois"],
    ["twice", "deux fois"],
    ["I drink coffee in a café", "boire un café a la cafeteria"],
    ["calm", "calme"],
    ["countryside", "campagne"],
    ["to overspend", "depenser trop"],
    ["MEET a deadline", "Respecter un délai"],
    ["catch up", "rattraper"],
    ["to delegate", "déléguer"],
    ["to allocate", "allouer"]
  ];


  constructor() { }

  private getRandomInt(max: number): number {
    return Math.floor(Math.random() * max);
  }

  get(): Quizz {
    const data = new Set<number>();
    while (data.size < 5) {
      const r = this.getRandomInt(this.list.length);
      data.add(r);
    }

    const num: number[] = Array.from(data);

    const pos = this.getRandomInt(5);

    return new Quizz(
      this.list[num[pos]][0],
      num.map( n => this.list[n][1]),
      pos
    );
  }
}
