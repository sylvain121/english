import { Component, OnInit } from '@angular/core';
import { WordsService } from './words.service';
import { Quizz } from './Quizz';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  quizz!: Quizz;
  answered: Set<number> = new Set<number>();
  speechEnable: boolean = false;

  constructor(private wordService: WordsService) { 
    if('speechSynthesis' in window) {
      this.speechEnable = true
    }
  }

  ngOnInit(): void {
    this.quizz = this.wordService.get();
  }

  private newQuizz() {
    this.answered.clear();
    this.quizz = this.wordService.get();
  }

  check(index: number) {

    if (index === this.quizz.goodAnswerposition) {
      this.newQuizz();
    } else {
      this.answered.add(index);
    }
  }

  wasWrong(index: number): boolean {
    return this.answered.has(index);
  }

  speak(word: string) {
    var msg = new SpeechSynthesisUtterance();
    msg.text = word;
    msg.lang = "en-US"; 
    window.speechSynthesis.speak(msg);
  }

}
